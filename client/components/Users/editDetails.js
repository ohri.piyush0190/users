import {useState, useEffect} from 'react';
import {useForm} from 'react-hook-form';
import { Box, Dialog, DialogTitle, TextField,Divider,DialogContent, DialogActions, IconButton, Button } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import {singleUser, updateProfile} from './../../actions/user_actions';



const email_regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;


const EditForm = ({ phone_num, reload,resetEdit}) =>{
      
      const {handleSubmit, errors, register, reset} = useForm();
      const [msg , setMessage] = useState("")
      const [user, setUser] = useState()

      useEffect(()=>{
            if(phone_num!="")
                  singleUser(phone_num).then(response => {
                        setUser(response.msg)
                  }).catch((err) => {
                        setMessage(err.msg)
                  })  
      }, [phone_num])

      const formSubmit=(data)=>{
            updateProfile(data, phone_num).then(response => {
                  setMessage(response.msg)
            }).catch((err) => {
                  setMessage(err.msg)
            })
      }
      const message = (mesag) =>{
           switch(mesag){
                case "success": return "User updated successfully !"
                default : return "Server Side error (please try again)"
          }
      } 
      useEffect(()=>{

      },[user])
      useEffect(()=>{
      }, [errors, msg])
      if(user)
            return <div>
            
                  <Dialog open={msg!==""}>
                        <DialogTitle><b>Message</b></DialogTitle>
                        <Divider></Divider>
                        <DialogContent>
                              <Box p={2}>
                                    <h4>{message(msg)}</h4>
                              </Box>
                        </DialogContent>
                        <Box display="flex">
                              <Box width="30%">
                              </Box>
                              <Box p={1}>
                                    <Button variant="contained" color="primary" onClick={()=>{setMessage(""); resetEdit(""); reload();}}>Close</Button>
                              </Box>
                        </Box>
                  </Dialog>
                        
                  <Dialog open={phone_num!=""}  aria-labelledby="customized-dialog-title" aria-labelledby="customized-dialog-title">
                        <DialogTitle>Edit User Details </DialogTitle>
                        <Divider/>
                        <Box id="close-btn-pos" >
                        <IconButton size="small" aria-label="close"  onClick={ () => resetEdit("")}>
                        <CloseIcon id="close-btn"/>
                        </IconButton>
                        </Box>
                        <DialogContent>

                        <form onSubmit={handleSubmit(formSubmit)}>
                              <Box display="flex" width="100%" p={1}>
                                    <Box width="50%" p={2}>
                                          <TextField
                                                 variant="outlined"
                                                 name="name"
                                                 label="Phone Number"
                                                disabled={true}
                                                value={phone_num}
                                                inputRef={register()}
                                          />
                                    </Box>
                                    <Box p={2}>
                                          <TextField
                                                variant="outlined"
                                                name="name"
                                                label="Name"
                                                placeholder="Your name"
                                                inputRef={register()}
                                                defaultValue={user.name}
                                          />
                                    </Box>
                              </Box>
                              <Box display="flex" width="100%" p={1}>
                                    <Box width="70%" p={2}>
                                          <TextField
                                                variant="outlined"
                                                name="email"
                                                label="E-mail"
                                                type="string"
                                                placeholder="email"
                                                inputRef={register({pattern:email_regex,required: true})}
                                                error={errors.email ?true:false}
                                                helperText={errors.email? "Invalid":""}
                                                defaultValue={user.email}
                                                fullWidth
                                          />
                                    </Box>
                                    <Box p={2}>
                                          <TextField
                                                variant="outlined"
                                                name="username"
                                                placeholder="Your user name"
                                                label="User name"
                                                inputRef={register()}
                                                defaultValue={user.username}
                                          />
                                    </Box>
                              </Box>
                              <Box display="flex" width="100%" p={1}>
                                    <Box width="100%" p={2}>
                                          <TextField
                                                variant="outlined"
                                                name="website"
                                                placeholder="link of website"
                                                label="Website Link"
                                                type="string"
                                                inputRef={register()}
                                                fullWidth
                                                defaultValue={user.website}
                                          />
                                    </Box>
                  
                              </Box>
                                          
                              <Box display="flex" width="100%" p={1}>
                                          <Box width="10%" p={2}>
                                          </Box>
                                          <Box width="30%" p={2}>
                                                <Button id="btn-style" type="Submit" variant="contained" color="primary">Modify</Button>
                                          </Box>
                                          <Box p={2}>
                                                <Button id="btn-style" variant="contained" color="secondary" onClick={()=>resetEdit("")}>Cancel</Button>
                                          </Box>
                                    </Box>
                                    
                        </form>

                        </DialogContent>
                  </Dialog>
                  </div>
      else  
            return <></>
      
}
export default EditForm