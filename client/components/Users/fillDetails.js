import {useState, useEffect} from 'react';
import {useForm} from 'react-hook-form';
import { Box, Dialog, DialogTitle, TextField,Divider,DialogContent, DialogActions, IconButton, Button } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import {addUser} from './../../actions/user_actions';




const email_regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

const fillform = ({type, setAction, reload}) =>{
      
      const {handleSubmit, errors, register, reset} = useForm();
      const [msg , setMessage] = useState("")

      const formSubmit=(data)=>{
            addUser(data).then(response => {
                  setMessage(response.msg)
                 
            }).catch((err) => {
                  setMessage(err.msg)
            })
      }
      const message = (mesag) =>{
           switch(mesag){
                case "exists": return "User already exists !"
                case "success": return "User added successfully !"
                default : return "Server Side error (please try again)"
          }
      } 

      useEffect(()=>{
      }, [errors, msg])
      
      return <div>
      
            <Dialog open={msg!==""}>
                  <DialogTitle><b>Message</b></DialogTitle>
                  <Divider></Divider>
                  <DialogContent>
                        <Box p={2}>
                              <h4>{message(msg)}</h4>
                        </Box>
                  </DialogContent>
                  <Box display="flex">
                        <Box width="30%">
                        </Box>
                        <Box p={1}>
                              <Button variant="contained" color="primary" onClick={()=>{setMessage(""); setAction(""); reload();}}>Close</Button>
                        </Box>
                  </Box>
            </Dialog>
                   
            <Dialog open={type!=""}  aria-labelledby="customized-dialog-title" aria-labelledby="customized-dialog-title">
                  <DialogTitle>Fill User Details </DialogTitle>
                  <Divider/>
                  <Box id="close-btn-pos" >
                    <IconButton size="small" aria-label="close"  onClick={ () => setAction("")}>
                      <CloseIcon id="close-btn"/>
                    </IconButton>
                  </Box>
                  <DialogContent>

                  <form onSubmit={handleSubmit(formSubmit)}>
                        <Box display="flex" width="100%" p={1}>
                              <Box width="50%" p={2}>
                                    <TextField
                                          variant="outlined"
                                          name="phone"
                                          label="Phone Number"
                                          type="number"
                                          inputRef={register({pattern: /^\d+$/,required: true , minLength:10, maxLength:10})}
                                          onInput={(e)=>{ if(e.target.value.length>0)e.target.value = Math.max(0, parseInt(e.target.value) ).toString().slice(0,10)}}
                                          error={errors.phone ?true:false}
                                          helperText={errors.phone? "Invalid":""}
                                          fullWidth
                                    />
                              </Box>
                              <Box p={2}>
                                    <TextField
                                          variant="outlined"
                                          name="name"
                                          label="Name"
                                          inputRef={register()}
                                    />
                              </Box>
                        </Box>
                        <Box display="flex" width="100%" p={1}>
                              <Box width="70%" p={2}>
                                    <TextField
                                          variant="outlined"
                                          name="email"
                                          label="E-mail"
                                          type="string"
                                          inputRef={register({pattern:email_regex,required: true})}
                                          error={errors.email ?true:false}
                                          helperText={errors.email? "Invalid":""}
                                          fullWidth
                                    />
                              </Box>
                              <Box p={2}>
                                    <TextField
                                          variant="outlined"
                                          name="username"
                                          label="User name"
                                          inputRef={register()}
                                    />
                              </Box>
                        </Box>
                        <Box display="flex" width="100%" p={1}>
                              <Box width="100%" p={2}>
                                    <TextField
                                          variant="outlined"
                                          name="website"
                                          label="Website Link"
                                          type="string"
                                          inputRef={register()}
                                          fullWidth
                                    />
                              </Box>
            
                        </Box>
                                    
                        <Box display="flex" width="100%" p={1}>
                                    <Box width="10%" p={2}>
                                    </Box>
                                    <Box width="30%" p={2}>
                                          <Button id="btn-style" type="Submit" variant="contained" color="primary">Submit</Button>
                                    </Box>
                                    <Box p={2}>
                                          <Button id="btn-style" variant="contained" color="secondary" onClick={()=>setAction("")}>Cancel</Button>
                                    </Box>
                              </Box>
                              
                  </form>

                  </DialogContent>
            </Dialog>
            </div>
      
}
export default fillform