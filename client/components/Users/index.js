import { useReducer, useEffect, useState } from "react";
import {Button, Box, Divider} from '@material-ui/core';
import FillDetails from './fillDetails';
import { getUsers } from "../../actions/user_actions";
import ListOfUsers from "./listOfUsers";
const UserPage =() =>{
     const [actionType, setAction] = useState("")
     const [usersList, setUsersList] = useState([]);
     const [reloadData , setReload] = useState(false);

     useEffect(()=>{
     }, [usersList])

     
      useEffect(() => {
            getUsers()
            .then(response => {
               setUsersList(response)
            })
            .catch((err) => {
            })
      },[reloadData])
       
      
      const handleReload = () =>{
            setReload(!reloadData);
      }
      
     const userForm = (formType) =>{
           switch(formType){
                  case "add": return <FillDetails type="Add" setAction={setAction} reload={handleReload}/>
                  case "" : return <></>
           }
     }

     return <div>

         <div className="heading">
          <div id="about">
            USERS
          </div>
          <div id="action">
            <Button id="btn-style" variant="contained" color="primary" onClick={()=>setAction("add")}> Add User </Button>
          </div>
           </div>  
         
           
         {userForm(actionType)}
         <ListOfUsers data={usersList} reload={handleReload}/>
         
            
         
     </div> 
}

export default UserPage;