import fetch from 'isomorphic-fetch';
 
export const singleUser = (id) => {
    return fetch(`${process.env.NEXT_PUBLIC_API}/user/${id}`, {
        method: 'GET',
        headers: {
           'Content-Type': 'application/json',
            Accept: 'application/json',
        }
    })
        .then(response => {
            return response.json();
        })
        .catch(err => console.log(err));
};
 
 
export const getUsers = () => {
    return fetch(`${process.env.NEXT_PUBLIC_API}/users/all`, {
            method:"GET",
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                Accept: 'application/json',
              },
        }).then(response =>{
            return response.json();
        }).catch(err => {
            return err;
            });
}
 
 
export const addUser = (newUser) => {
    
        return fetch(`${process.env.NEXT_PUBLIC_API}/addUser`, {
            method: 'POST',
            headers: {
               'Content-Type': 'application/json',
                Accept: 'application/json',
            },
            body: JSON.stringify(newUser)
        })
            .then(response => {
                return response.json();
            })
            .catch(err => console.log(err));
};
 
export const removeUser = (contact) => {
    return fetch(`${process.env.NEXT_PUBLIC_API}/removeUser/${contact.phone}`, {
        method: 'POST',
        headers: {
           'Content-Type': 'application/json',
            Accept: 'application/json',
        },
        body: JSON.stringify(contact)
    })
        .then(response => {
            return response.json();
        })
        .catch(err => console.log(err));
}
 
export const updateProfile = (details, phone)=>{
    return fetch(`${process.env.NEXT_PUBLIC_API}/user/update/${phone}`, {
        method: 'POST',
        headers: {
           'Content-Type': 'application/json',
            Accept: 'application/json',
        },
        body: JSON.stringify(details)
    })
        .then(response => {
            return response.json();
        })
        .catch(err => console.log(err));
}
 

