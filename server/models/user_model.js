const mongoose = require('mongoose');

const userSchema  = mongoose.Schema({
      name:{
            type:String,
            trim:true,
            max:32,
            default:null
      },
      username:{
            type:String,
            max:32,
            default:null  
      },
      email:{
            type:String,
            trim:true,
            max:32,
            default:"null"
      },
      phone:{
            type:String,
            required:true,
            unique:true,
            max:20
      },
      website:{
            type:String,
            trim:true,
            max:64,
            default:null
      },
      del_flag:{
            type:Boolean,
            default:false
      }
})

module.exports = mongoose.model("User", userSchema);