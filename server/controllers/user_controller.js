const { response } = require("express");
const User = require("../models/user_model");


module.exports.update_user_profile = (req, res)=>{

  const {
    name,
    username,
    email,
    website,
    } = req.body;
  User.findOneAndUpdate({phone:req.params.id}, {name, username, email, phone:req.params.id, website,del_flag:false}, (err, result)=>{
    if(err){
     return res.status(404).json({msg:"error"})
    }
    else{
      return res.status(200).json({msg:"success"})
    }
  })
}

module.exports.single_user = (req, res) => {
     User.findOne({phone:req.params.id}, (err, result)=>{
      if(err){
        return res.status(400).json({
          msg: err
        })
      }
      res.status(200).json({
        msg: result
      })
     })  
}


exports.get_user = (req, res) => {
    User.find({del_flag:false} , (err, result)=>{
        if(err)
            return res.status(404).json(err)
        else
           return  res.status(200).json(result)
    })
}


exports.create_user = (req, res) => {
    const {
      name,
      username,
      email,
      phone,
      website,
    } = req.body;
    User.findOneAndUpdate(
          {phone:phone}, 
          {name:name,username:username,
            email:email,website:website,
            del_flag:false},
      (err, result) => {

       if(result){
         res.status(200).json({msg:"success"})
       }
       else{
        const addUser = new User({
            name,
            username,
            email,
            phone,
            website,
        })
        addUser.save((err, result) => {
            if(err){
              return res.status(400).json({
              error: "error"
              })
        } else{
            res.status(200).json({msg:"success"})
        }})
       }

    })

}


exports.delete_user = (req, res) => {
    const {
      name,
      username,
      email,
      phone,
      website} = req.body;
    User.findOneAndUpdate({phone:req.params.id}, {name:name,username:username,email:email,phone:phone,website:website, del_flag:true},(err, result)=>{
        if(err)
            res.status(404).json({msg:"error"})
        else{
            res.status(200).json({msg:"success"})
          }
    })
}

