const express = require("express");
const router = express.Router();

const { single_user,get_user,create_user,delete_user,update_user_profile } = require("../controllers/user_controller");

router.get("/user/:id",   single_user);
router.get('/users/all',   get_user);

router.post('/user/update/:id', update_user_profile);
router.post('/addUser',   create_user);
router.post('/removeUser/:id',  delete_user);

module.exports = router;